﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class ThrustModifier : IModifier
    {
        public static ThrustModifier Default = new ThrustModifier();

        /// <summary>prevents thrust in a straight line</summary> 
        /// <remarks>2 = -1 to +1</remarks> 
        public float Alterations = 2; 
        public void Modify(UiElement element) { 
            if (element.Velocity.X > 0.01 || element.Velocity.X < -0.01 || element.Velocity.Y > 0.01 || element.Velocity.Y < -0.01) { 
                // Thrust will be the mirror of the direction the element moves. 
                Vector2 thrustVelocity = Vector2.Zero - element.Velocity; 
                Vector2 variation = Access.RandomVector2(new Vector2(this.Alterations)); 
                variation.X -= Alterations / 2; 
                variation.Y -= Alterations / 2;
                //Access.CreateFadingParticle(element.Location, thrustVelocity + variation); 
                Access.CreateFadingParticle(element.Center, thrustVelocity + variation); 
            } 
        }
    }
}
