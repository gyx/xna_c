﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Reacts to Collisions</summary>
    class ReactToImpactModifier : IModifier
    {
        /// <summary>Defines the shape of a method to call</summary> 
        //delegate turns turns method into variable
        public delegate void CollisionHandler(UiElement projectile, UiElement target); 

        /// <summary>If there was a collision. This passes it on.</summary> 
        public event CollisionHandler CollisionDetected; 

        /// <summary>What are the targets of this projectile.</summary> 
        public UiElements Targets; 

        /// <summary>Provides a common reactor for shot to enemy collisions</summary> 
        //DEPRECATED
        //public static ReactToImpactModifier TargetEnemiesDefault = new ReactToImpactModifier() 
        //{ Targets = Access.Enemies }; 

        /// <summary>Establishes the default reactor</summary> 
        public ReactToImpactModifier() 
        { this.CollisionDetected += new CollisionHandler(CollisionDetectedReactor); } 

        /// <summary>Default Reaction is to delete the colliding elements</summary> 
        private void CollisionDetectedReactor(UiElement projectile, UiElement target) 
        { 
            projectile.DeleteMe = true;
            target.DeleteMe = true; 
        } 

        //public void Modify(UiElement element) 
        //{ 
            // it is possible that no targets are attached 
        //    if (Targets == null) return;
        //    foreach (UiElement target in Targets) 
        //    { 
        //        if (Access.Distance(target, element) < target.ImpactRadius + element.ImpactRadius) 
        //        { 
        //            if (CollisionDetected != null) CollisionDetected(element, target); 
        //        } 
        //    } 
        //}

        public void Modify(UiElement element) 
        { 
            // it is possible that no targets are attached 
            if (Targets == null) return; 
            for(int i = Targets.Count - 1; i > -1; i--) 
            {
                if (Targets.Count == 0) return; 
                if (Targets.Count <= i) continue; 
                UiElement target = Targets[i]; 
                if (Access.Distance(target, element) < target.ImpactRadius + element.ImpactRadius) 
                { if (CollisionDetected != null) CollisionDetected(element, target); } 
            } 
        }

        /// <summary>Provides a common reactor for shot to enemy collisions</summary> 
        public static ReactToImpactModifier TargetEnemiesDefault = new ReactToImpactModifier() 
        {
            Targets = Access.Enemies, CollisionDetected = Access.HandleEnemyImpact 
        };

    }
}
