﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Moves the element with the arrows.</summary>
    class ImpulseOnArrowModifier : IModifier
    {
        public void Modify(UiElement element) { 
            KeyboardState keyboard = Keyboard.GetState(); 
            if (keyboard.IsKeyDown(Keys.Down)) { 
                element.Velocity.Y += element.Impulse; 
            } 
            if (keyboard.IsKeyDown(Keys.Right)) {
            element.Velocity.X += element.Impulse; 
            } 
            if (keyboard.IsKeyDown(Keys.Up)) { 
                element.Velocity.Y -= element.Impulse; 
            } 
            if (keyboard.IsKeyDown(Keys.Left)) { 
                element.Velocity.X -= element.Impulse; 
            } 
        } 
    }
}
