﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class FadeOutModifier : IModifier 
    {
        public byte Reducer = 1;

        public static FadeOutModifier Default = new FadeOutModifier();

        public static FadeOutModifier DefaultFast = new FadeOutModifier() { Reducer = 5 };

        //DEPRECATED
        //public void Modify(UiElement element) 
        //{ 
        //    if (element.Color.A == 0) 
        //    { 
        //        element.DeleteMe = true; 
        //        return; 
        //    } 
        //    element.Color.A--; 
        //    if (element.Color.B > 0) element.Color.B--; 
        //    if (element.Color.G > 0) element.Color.G--; 
        //    if (element.Color.R > 0) element.Color.R--; 
        //} 

        public void Modify(UiElement element) 
        { 
            if (element.Color.A < Reducer) 
            { 
                element.DeleteMe = true; 
                return; 
            } 
            if (element.Color.A >= Reducer) element.Color.A -= Reducer; 
            if (element.Color.B >= Reducer) element.Color.B -= Reducer;
            if (element.Color.G >= Reducer) element.Color.G -= Reducer; 
            if (element.Color.R >= Reducer) element.Color.R -= Reducer; 
        }
    }
}
