﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Marks an object for deletion /// 
    /// if it moves off the screen.</summary> 
    class DeleteOffScreenModifier : IModifier { 
        /// <summary>Provides me with </summary> 
        public static DeleteOffScreenModifier Default = new DeleteOffScreenModifier(); 
        public void Modify(UiElement element) { 
            if (element.Location.X > Access.ScreenSize.X) {
                element.DeleteMe = true; 
                return; 
            } 
            if (element.Location.Y > Access.ScreenSize.Y) { 
                element.DeleteMe = true; 
                return; 
            } 
            if (element.Location.X + element.Size.X < 0) { 
                element.DeleteMe = true; 
                return; 
            } 
            if (element.Location.Y + element.Size.Y < 0) { 
                element.DeleteMe = true; 
                return; 
            } 
        } 
    }
}
