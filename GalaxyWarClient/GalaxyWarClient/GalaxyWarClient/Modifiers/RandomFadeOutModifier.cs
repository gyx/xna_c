﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class RandomFadeOutModifier : IModifier
    {
        public byte Reducer = 15; 
        public static RandomFadeOutModifier Default = new RandomFadeOutModifier();
        public void Modify(UiElement element) 
        { 
            if (element.Color.A < Reducer) 
            { 
                element.DeleteMe = true; 
                return; 
            }

            int reducer = Access.Random.Next(Reducer); 
            if (element.Color.A >= Reducer) element.Color.A -= Reducer; 

            reducer = Access.Random.Next(Reducer); 
            if (element.Color.B >= Reducer) element.Color.B -= Reducer; 

            reducer = Access.Random.Next(Reducer); 
            if (element.Color.G >= Reducer) element.Color.G -= Reducer; 

            reducer = Access.Random.Next(Reducer); 
            if (element.Color.R >= Reducer) element.Color.R -= Reducer; 
        }
    }
}
