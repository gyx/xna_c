﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>When elements move off screen, it shows up on the other side.</summary> 
    class WrapModifier : IModifier { 
        /// <summary>Default copy of WrapModifier</summary> 
        public static WrapModifier Default = new WrapModifier(); 
        public void Modify(UiElement element) { 
            if (element.Velocity.X > 0 // moving Right 
                && element.Location.X > Access.ScreenSize.X) 
            { 
                element.Location.X = 0 - element.Size.X; 
            } 
            if (element.Velocity.Y > 0 // moving Down 
                && element.Location.Y > Access.ScreenSize.Y) 
            { 
                element.Location.Y = 0 - element.Size.Y; 
            } 
            if (element.Velocity.X < 0 // moving Left 
                && element.Location.X < 0 - element.Size.X) 
            { 
                element.Location.X = Access.ScreenSize.X; 
            } 
            if (element.Velocity.Y < 0 // moving Up 
                && element.Location.Y < 0 - element.Size.Y) 
            { 
                element.Location.Y = Access.ScreenSize.Y; 
            } 
        } 
    }
}
