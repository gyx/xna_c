﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class ImpulseBoostModifier : IModifier
    {
        public Vector2 Modifier = new Vector2(0, -1); 
        public static ImpulseBoostModifier Up = new ImpulseBoostModifier() { Modifier = new Vector2(0, -1) }; 
        public void Modify(UiElement element) { element.Velocity += Modifier * element.Impulse; }
        public static ImpulseBoostModifier Down = new ImpulseBoostModifier() { Modifier = new Vector2(0, 1) };
    }
}
