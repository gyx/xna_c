﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Slows a ship down</summary>
    class FrictionModifier : IModifier
    {
        public static FrictionModifier Default = new FrictionModifier();

        /// <summary>Slow Down by how much percent</summary>
        /// <remarks>0.05 = 5%, the default.</remarks>
        public float Friction = 0.05f;

        /// <summary>Updates the friction reduction</summary>
        public void Modify(UiElement element) { element.Velocity *= 1 - this.Friction; }
    }
}
