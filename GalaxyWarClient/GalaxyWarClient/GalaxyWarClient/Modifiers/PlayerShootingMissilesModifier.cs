﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class PlayerShootingMissilesModifier : IModifier
    {
        public Keys FireKey = Keys.Space; 
        public void Modify(UiElement element) { 
            //if (Access.KeyboardNow.IsKeyDown(this.FireKey)) { 
            if (Access.CheckFirstKeyPress(this.FireKey)) {
                UiElement shot1 = Access.PlayerShots.Add();
                shot1.Velocity.Y = -6f + element.Velocity.Y;
                shot1.Velocity.X = element.Velocity.X;
                shot1.Impulse = 0.2f;
                //shot1.Image = Access.Images["PlayerMissile01"]; 
                shot1.Image = Access.Images["PlayerMissile02"];
                //shot.Location = element.Location;
                Vector2 offset1 = element.Center;
                offset1.X -= 35;
                shot1.Location = offset1;

                shot1.Modifiers.Add(ThrustModifier.Default);
                //shot.Modifiers.Add(ThrustModifier.Default);
                shot1.Modifiers.Add(ImpulseBoostModifier.Up);
                shot1.Modifiers.Add(FrictionModifier.Default);
                shot1.Modifiers.Add(ReactToImpactModifier.TargetEnemiesDefault);

                UiElement shot2 = Access.PlayerShots.Add();
                shot2.Velocity.Y = -6f + element.Velocity.Y;
                shot2.Velocity.X = element.Velocity.X;
                shot2.Impulse = 0.2f;
                //shot2.Image = Access.Images["PlayerMissile01"];
                shot2.Image = Access.Images["PlayerMissile02"];
                //shot.Location = element.Location;
                Vector2 offset2 = element.Center;
                offset2.X += 22;
                shot2.Location = offset2;

                shot2.Modifiers.Add(ThrustModifier.Default);
                //shot.Modifiers.Add(ThrustModifier.Default);
                shot2.Modifiers.Add(ImpulseBoostModifier.Up);
                shot2.Modifiers.Add(FrictionModifier.Default);
                shot2.Modifiers.Add(ReactToImpactModifier.TargetEnemiesDefault);

                //MediaPlayer.Play(Access.Sounds["missile"]);
                Access.Sounds["missile"].Play();
            } 
        }
    }
}
