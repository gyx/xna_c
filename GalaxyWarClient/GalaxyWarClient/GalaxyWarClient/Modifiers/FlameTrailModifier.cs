﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class FlameTrailModifier : IModifier
    {
        public int RedMax = 100; 
        public int GreenMax = 75; 
        public int BlueMax = 25; 
        public int Range = 50;
        public void Modify(UiElement element) 
        { 
            Ceiling(element.Color); 
            UiElement part = Access.Particles.Add(); 
            part.Modifiers.Add(RandomFadeOutModifier.Default); 
            //part.Modifiers.Add(WobbleModifier.Default); 
            part.Location = element.Location; 
            part.Image = Access.Images["Particle01"]; 
            part.Size *= 5 * Access.RandomFloat; 
            part.Color = new Color( 
                Access.Random.Next(Range) + RedMax, 
                Access.Random.Next(Range) + GreenMax, 
                Access.Random.Next(Range) + BlueMax); 
        } 
        
        private void Ceiling(Color c) { 
            if (RedMax > c.A) RedMax = c.A; 
            if (GreenMax > c.A) GreenMax = c.A; 
            if (BlueMax > c.A) BlueMax = c.A; 
            if (Range > c.A) Range = c.A;
        }
    }
}
