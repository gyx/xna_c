﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Handles Enemy Shooting</summary>
    class EnemyShootingModifier : IModifier
    {
        public int Min = 100; 
        public int Range = 50; 
        private int counter = 0;

        private void setCounter() 
        { this.counter = Min + Access.Random.Next(Range); }

        private EnemyShootingModifier(int min, int range) 
        { 
            this.Min = min; 
            this.Range = range; 
            this.setCounter(); 
        }

        /// <summary>Creates a new Enemy Shooter</summary> 
        /// <returns></returns> 
        //public static EnemyShootingModifier Create() 
        //{ return new EnemyShootingModifier(); } 

        public static EnemyShootingModifier Create(int min, int range) { return new EnemyShootingModifier(min, range); }
        
        public void ModifyRandom(UiElement element) 
        { 
            if (Access.RandomFloat > 0.98f) 
            { 
                UiElement shot = Access.EnemyShots.Add(); 
                shot.Image = Access.Images["EnemyMissile01"]; 
                shot.Location = element.Location; 
            } 
        }

        public void Modify(UiElement element) 
        { 
            if (this.counter-- < 0) 
            { 
                this.setCounter(); 
                UiElement shot = Access.EnemyShots.Add(); 
                shot.Image = Access.Images["EnemyMissile01"]; 
                shot.Location = element.Location;
                shot.Modifiers.Add(ImpulseBoostModifier.Down); 
                shot.Modifiers.Add(DeleteOffScreenModifier.Default); 
                shot.Modifiers.Add(ThrustModifier.Default);
            } 
        }

    }
}
