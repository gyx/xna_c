﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    class WobbleModifier : IModifier
    {
        public float Range = 1; 
        public static WobbleModifier Default = new WobbleModifier(); 
        public void Modify(UiElement element) 
        { 
            element.Location.X += Access.RandomFloat * (Range * 2) - Range; 
            element.Location.Y += Access.RandomFloat * (Range * 2) - Range; 
        }
    }
}
