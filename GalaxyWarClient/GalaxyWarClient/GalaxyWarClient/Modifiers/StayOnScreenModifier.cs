﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    /// <summary>Keeps an element on the screen.</summary>
    class StayOnScreenModifier : IModifier
    {
        public void Modify(UiElement element) {
            Vector2 loc = element.Location; 
            Vector2 size = Access.ImageSize(element.Image); 
            Vector2 otherCorner = size + loc; if (loc.X < 0) { element.Location.X = 0; } 
            if (loc.Y < 0) { element.Location.Y = 0; } 
            if (otherCorner.X > Access.ScreenSize.X) { element.Location.X = Access.ScreenSize.X - size.X; } 
            if (otherCorner.Y > Access.ScreenSize.Y) { element.Location.Y = Access.ScreenSize.Y - size.Y; }
        }
    }
}
