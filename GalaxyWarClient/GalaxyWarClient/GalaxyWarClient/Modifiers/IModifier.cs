﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Modifiers
{
    public interface IModifier
    {
        void Modify(UiElement element);
    }
}
