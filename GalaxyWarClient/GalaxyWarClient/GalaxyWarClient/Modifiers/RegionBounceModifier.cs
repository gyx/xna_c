﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace GalaxyWarClient.Modifiers
{
    class RegionBounceModifier : IModifier
    {
        public Vector2 Region = Vector2.Zero;
        private static RegionBounceModifier screenDefault;

        public static RegionBounceModifier ScreenDefault
        {
            get
            {
                if (screenDefault == null)
                {
                    screenDefault = new RegionBounceModifier();
                    screenDefault.Region = Access.ScreenSize;
                }
                return screenDefault;
            }
        }

        private static RegionBounceModifier topHalfDefault; 

        public static RegionBounceModifier TopHalfDefault 
        { 
            get 
            { 
                if (topHalfDefault == null) 
                { 
                    topHalfDefault = new RegionBounceModifier(); 
                    topHalfDefault.Region.X = Access.ScreenSize.X; 
                    topHalfDefault.Region.Y = Access.ScreenSize.Y / 2; 
                } 
                return topHalfDefault; 
            } 
        }

        public void Modify(UiElement element) 
        { 
            Vector2 mom = element.Velocity; 
            Vector2 loc = element.Location; 
            Vector2 size = element.Size; 
            Vector2 otherCorner = size + loc; 
            if (mom.X < 0 && loc.X < 0) 
            { 
                element.Velocity.X = 0 - element.Velocity.X; 
            } 
            else if (mom.X > 0 && otherCorner.X > Region.X) 
            { 
                element.Velocity.X = 0 - element.Velocity.X; 
            } 
            if (mom.Y < 0 && loc.Y < 0) 
            { 
                element.Velocity.Y = 0 - element.Velocity.Y; 
            } 
            else if (mom.Y > 0 && otherCorner.Y > Region.Y) 
            { 
                element.Velocity.Y = 0 - element.Velocity.Y; 
            } 
        }
    }
}
