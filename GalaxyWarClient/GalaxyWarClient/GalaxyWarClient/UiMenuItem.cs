﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GalaxyWarClient
{
    class UiMenuItem : UiElement
    {
        public string Text = "Undecided Menu Item"; 
        public Color TextColor = Color.White;
        public static Color BackgroundColor = new Color(50, 50, 50, 220); 
        public static Color HighlightColor = new Color(50, 70, 50, 220);
        public delegate void ClickHandler(string key);
        public event ClickHandler Clicked;
        private string key = "";
        
        //public UiMenuItem(string text) 
        //{ 
        //    this.Text = text; 
        //    this.Color = new Color(50, 50, 50, 220); 
        //    this.Image = Access.Images["Particle01"]; 
        //    this.Size = new Vector2(300, 50); 
        //    this.Location.X = 250; 
        //} 
        
        public override void Paint() 
            {
                base.Paint(); 
                Access.SpriteBatch.DrawString(
                    Access.Display.MainFont, 
                    this.Text, 
                    this.Location, 
                    this.TextColor); 
        }

        public override void Modify() 
        { 
            base.Modify(); 
            Vector2 mouse = Access.Display.MouseLocation;
            if (mouse.X > this.Location.X &&
                mouse.Y > this.Location.Y &&
                mouse.X < this.Location.X + this.Size.X &&
                mouse.Y < this.Location.Y + this.Size.Y)
            {
                this.Color = HighlightColor;
                if (Access.Display.MousePrevious.LeftButton == ButtonState.Pressed
                    && Access.Display.MouseNow.LeftButton == ButtonState.Released
                    && this.Clicked != null)
                {
                    this.Clicked(this.key);
                }
            }
            else
            { this.Color = BackgroundColor; }
        }

        public static void AdjustMenus(UiElements menuItems) 
        { 
            float separation = (Access.ScreenSize.Y - StandardSize.Y) / (menuItems.Count + 1); 
            float position = 1; foreach (UiElement element in menuItems) 
            { element.Location.Y = position++ * separation; } 
        } 
        
        public static Vector2 StandardSize = new Vector2(300, 50);

        public UiMenuItem(string key, string text)
        {
            this.key = key;
            this.Text = text;
            this.Image = Access.Images["Particle01"];
            this.Size = StandardSize;
            this.Location.X =
                (Access.ScreenSize.X - StandardSize.X) / 2;
        }
    }
}
