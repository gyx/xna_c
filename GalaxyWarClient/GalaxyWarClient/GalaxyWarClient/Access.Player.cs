﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace GalaxyWarClient
{
    partial class Access 
    { 
        public class Player 
        { 
            //public static UiElement Active = new UiElement();
            public static UiElement Active;
            public static int Score = 0;
            public static int Health = 100;
            public static int Lives = 3;

            public static UiElement Create() 
            { 
                Active = new UiElement();
                Active.Image = Access.Images["Player01"]; 
                //Active.Size *= 0.25f; 
                Active.Modifiers.Add(new Modifiers.StayOnScreenModifier()); 
                Active.Modifiers.Add(new Modifiers.FrictionModifier()); 
                Active.Modifiers.Add(new Modifiers.ImpulseOnArrowModifier()); 
                Active.Modifiers.Add(new Modifiers.PlayerShootingMissilesModifier());

                Modifiers.ReactToImpactModifier impact = new Modifiers.ReactToImpactModifier();
                impact.CollisionDetected += CollisionDetected;
                impact.Targets = Access.EnemyShots;
                Active.Modifiers.Add(impact);

                Active.Location.X = (Access.ScreenSize.X - Active.Size.X) / 2; 
                Active.Location.Y = Access.ScreenSize.Y - Active.Size.Y; 
                return Active; 
            }

            private static void CollisionDetected(UiElement ship, UiElement projectile) 
            { 
                projectile.DeleteMe = true;
                UiElement.Disorientation = 4;
                float distance = Access.Distance(ship, projectile); 
                Vector2 alter = (ship.Center - projectile.Center) / distance; 
                ship.Velocity += alter * 5;
                Health -= 20;
                //MediaPlayer.Play(Access.Sounds["explosion"];
                Access.Sounds["explosion"].Play();
                //Health -= (int)projectile.Damage;
                if (Health < 1) 
                {
                    Access.Particles.AddRange(Access.CreatePixelPerfectExplosion(ship));
                    Create();
                    Health = 100; 
                    if (--Lives <0)
                    {
                        Active.Modifiers.Clear();
                        Access.Display.CentralMessage = "Game Over";
                        Access.Sounds["evillaugh"].Play();
                    }
                    UiElement.Disorientation = 12;
                    Access.EnemyShots.Clear();
                }
            }
        } 
    }
}
