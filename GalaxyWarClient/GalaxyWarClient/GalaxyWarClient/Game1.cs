using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GalaxyWarClient
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        /// <summary>Manages the player object</summary> 
        //UiElement playerElement = new UiElement();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //To end game from menu
            Access.CurrentGame = this;
        }

        // Create a Rectangle that will define
	    // the limits for the main game screen.
	    Rectangle mainFrame;

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            // spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Access.SpriteBatch = new SpriteBatch(GraphicsDevice);

            Access.Images.Add("Player01", this.Content.Load<Texture2D>("Player01")); 
            Access.Images.Add("Star01", this.Content.Load<Texture2D>("Star01")); 
            Access.Images.Add("PlayerMissile01", this.Content.Load<Texture2D>("PlayerMissile01"));
            Access.Images.Add("PlayerMissile02", this.Content.Load<Texture2D>("shot2"));
            Access.Images.Add("Particle01", this.Content.Load<Texture2D>("Particle01"));
            Access.Images.Add("Enemy01", this.Content.Load<Texture2D>("Enemy01"));
            Access.Images.Add("Enemy02", this.Content.Load<Texture2D>("Enemy02"));
            Access.Images.Add("Enemy03", this.Content.Load<Texture2D>("Enemy03"));
            Access.Images.Add("EnemyMissile01", this.Content.Load<Texture2D>("EnemyMissile01"));
            Access.Images.Add("Background", this.Content.Load<Texture2D>("nebula_blue"));

            Access.Sounds.Add("missile", this.Content.Load<SoundEffect>("missile"));
            Access.Sounds.Add("explosion", this.Content.Load<SoundEffect>("explosion"));
            Access.Sounds.Add("evillaugh", this.Content.Load<SoundEffect>("Evillaugh"));
            Access.Songs.Add("soundtrack", this.Content.Load<Song>("soundtrack"));

            Access.Display.MouseCursor = this.Content.Load<Texture2D>("Cursor01");

            mainFrame = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            
            Access.Display.MainFont = this.Content.Load<SpriteFont>("Main");

            Access.Player.Create();

            //this.playerElement.Image = this.Content.Load<Texture2D>("Player01");
            //Access.Player.Active.Image = Access.Images["Player01"];
            //Access.Player.Active.Size *= 0.25f;
            //this.playerElement.Modifiers.Add(new Modifiers.StayOnScreenModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.WrapModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.FrictionModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.ImpulseOnArrowModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.PlayerShootingMissilesModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.ThrustModifier());
            //Access.Player.Active.Modifiers.Add(new Modifiers.ThrustModifier());

            //Access.Player.Active.Location.X = (Access.ScreenSize.X - Access.Player.Active.Size.X) / 2;
            //Access.Player.Active.Location.Y = Access.ScreenSize.Y - Access.Player.Active.Size.Y;

            //Texture2D starImage = this.Content.Load<Texture2D>("Star01");
            for (int i = 0; i < 50; i++) {
                UiElement star = Access.Stars.Add(); 
                //star.Image = starImage; 
                star.Image = Access.Images["Star01"];
                star.Location = Access.RandomVector2(Access.ScreenSize); 
                star.Velocity.Y = 1 + Access.RandomFloat * 2; 
                //star.Size.Y /= 2; 
                //star.Size.X /= 2;
                star.Size /= 2;
                star.Size *= star.Velocity.Y; 
                star.Color = new Color(Access.RandomFloat, Access.RandomFloat, Access.RandomFloat); 
                star.Modifiers.Add(Modifiers.WrapModifier.Default); 
            }
            //Access.Levels.Prepare();
            //Access.CreateEnemy1();
            //Access.CreateEnemy2();

            MediaPlayer.Play(Access.Songs["soundtrack"]);
            MediaPlayer.IsRepeating = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            Access.KeyboardOld = Access.KeyboardNow; 
            Access.KeyboardNow = Keyboard.GetState();
            Access.Display.MousePrevious = Access.Display.MouseNow;
            Access.Display.MouseNow = Mouse.GetState();

            if (Access.CheckFirstKeyPress(Keys.Escape)) 
            { 
                Access.Display.InMenu = !Access.Display.InMenu; 
            }
            if (Access.Display.InMenu)
            {
                Access.Display.Menu.Modify();
            }
            else
            {
                Access.Levels.Update();
                Access.Player.Active.Modify();
                Access.PlayerShots.Modify();
                Access.Enemies.Modify();
                Access.EnemyShots.Modify();
                Access.Particles.Modify();
            }
            Access.Stars.Modify();
            UiElement.Disorientation -= 0.1f;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.Clear(Color.Black);

            Access.SpriteBatch.Begin();
                Access.SpriteBatch.Draw(Access.Images["Background"], mainFrame, Color.White);
                Access.Stars.Paint();
                Access.Particles.Paint();
                Access.Enemies.Paint();
                Access.Player.Active.Paint();
                Access.PlayerShots.Paint();
                Access.EnemyShots.Paint();
                Access.Display.Paint();

                //if (Access.Display.Dialog != null)
                //{ Access.Display.Dialog.Paint(); }

                if (Access.Display.InMenu)
                {
                    Access.Display.Menu.Paint(); 
                    Access.SpriteBatch.Draw(
                        Access.Display.MouseCursor, 
                        Access.Display.MouseLocation, 
                        Color.White);
                }
            Access.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
