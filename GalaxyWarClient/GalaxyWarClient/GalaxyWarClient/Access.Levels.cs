﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaxyWarClient.Reactors;
using Microsoft.Xna.Framework;

namespace GalaxyWarClient
{
    partial class Access 
    { 
        /// <summary>Manages Levels for the game</summary> 
        public class Levels 
        { 
            /// <summary>Current Level</summary> 
            public static int CurrentLevel = 0;
            
            /// <summary>Determines needed level updates</summary> 
            public static void Update() 
            { 
                if (Access.Enemies.Count == 0) 
                { 
                    CurrentLevel++;
                    int e3 = CurrentLevel / 5;
                    int e2 = CurrentLevel / 4; 
                    int e1 = CurrentLevel - (e2 * 4) - (e3 * 5); 
                    for (int i = 0; i < e1; i++) { Access.CreateEnemy1(); } 
                    for (int i = 0; i < e2; i++) { Access.CreateEnemy2(); }
                    for (int i = 0; i < e3; i++) { Access.CreateEnemy3(); } 
               } 
            }

            /// <summary>Determines needed level updates</summary>
            //public static void Update()
            //{
            //    Reactor.React();
            //}

            public static void Prepare()
            {
                Reactor.Reactions.Clear();
                // Immediately Display "3"
                Reaction reaction = new Reaction();
                reaction.Condition = new WaitCondition(0);
                reaction.Action = new SetMessageAction(
                "3", Color.White);
                Reactor.Reactions.Add(reaction);
                // In 2 Seconds Display "2"
                reaction = new Reaction();
                reaction.Condition = WaitCondition.Default;
                reaction.Action = new SetMessageAction(
                "2", Color.White);
                Reactor.Reactions.Add(reaction);
                // In 2 seconds display "1"
                reaction = new Reaction();
                reaction.Condition = WaitCondition.Default;
                reaction.Action = new SetMessageAction(
                "1", Color.White);
                Reactor.Reactions.Add(reaction);
                // In 2 Seconds Clear Message
                reaction = new Reaction();
                reaction.Condition = WaitCondition.Default;
                reaction.Action = new SetMessageAction(
                "", Color.White);
                Reactor.Reactions.Add(reaction);
                // Immediately Add Enemies
                reaction = new Reaction();
                reaction.Condition = new WaitCondition(0);
                reaction.Action = StandardEnemyLoadAction.Default;
                Reactor.Reactions.Add(reaction);
                // When enemies = 0, Increment level
                reaction = new Reaction();
                reaction.Condition = NoEnemiesLeftCondition.Default;
                reaction.Action = NextLevelAction.Default;
                Reactor.Reactions.Add(reaction);
                // immediately Declare level
                reaction = new Reaction();
                reaction.Condition = WaitCondition.Default;
                reaction.Action = new SetMessageAction(
                "Level 2", Color.White);
                Reactor.Reactions.Add(reaction);
                // After 2 seconds, clear text
                reaction = new Reaction();
                reaction.Condition = WaitCondition.Default;
                reaction.Action = new SetMessageAction(
                "", Color.White);
                Reactor.Reactions.Add(reaction);
                // Immediately Add Enemies
                reaction = new Reaction();
                reaction.Condition = new WaitCondition(0);
                reaction.Action = StandardEnemyLoadAction.Default;
                Reactor.Reactions.Add(reaction);
            }

            public static ReactorCore Reactor = new ReactorCore();
        } 
    }
}
