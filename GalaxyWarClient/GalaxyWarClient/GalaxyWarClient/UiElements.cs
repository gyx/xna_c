﻿using GalaxyWarClient.Modifiers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient
{
    /// <summary>Handles a collection of objects</summary> 
    class UiElements : List<UiElement>
    {
        /// <summary>Modifies all the elements</summary> 
        public void Modify()
        {
            //using for because foreach can't delete in a loop
            for (int i = this.Count - 1; i > -1; i--)
            {
                UiElement element = this[i];
                element.Modify();
                if (element.DeleteMe)
                {
                    this.Remove(element);
                }
            }
        }

        /// <summary>Adds a new element</summary> 
        public UiElement Add()
        {
            UiElement element = new UiElement();
            this.Add(element); return element;
        }
        /// <summary></summary> 
        public void Paint()
        {
            foreach (UiElement element in this) { element.Paint(); }
        }

        //add modifier to list
        public IModifier AddModifierToAll(IModifier modifier) 
        { 
            foreach (UiElement element in this) { element.Modifiers.Add(modifier); } 
            return modifier; 
        } 
        
        //move all elements by offset (location)
        public void MoveAll(Vector2 offset) 
        { 
            foreach (UiElement element in this) { element.Location += offset; } 
        }

        //change all elements by offset (velocity)
        public void AlterVelocityToAll(Vector2 offset) 
        { 
            foreach (UiElement element in this) { element.Velocity += offset; } 
        }
    }
}
