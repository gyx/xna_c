﻿using GalaxyWarClient.Modifiers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient
{
    ///<summary>Provides access to common data and functions</summary> 
    partial class Access
    {
        //This will give us a reference to a Game object, which Game1 is, since it inherits from it
        public static Game CurrentGame;

        /// <summary>X = Width, Y = Height of the View</summary> 
        public static Vector2 ScreenSize = new Vector2(800, 480);

        /// <summary>gets a vector from an image's size</summary> 
        /// <param name="image"></param> /// <returns></returns> 
        public static Vector2 ImageSize(Texture2D image)
        {
            return new Vector2(image.Width, image.Height);
        }

        /// <summary>provides Random numbers</summary> 
        public static Random Random = new Random(); 

        /// <summary>provides random decimals</summary> 
        public static float RandomFloat { 
            get { return (float)Random.NextDouble(); } 
        } 

        /// <summary>Random Vector between 0 and limit's x/y</summary> 
        public static Vector2 RandomVector2(Vector2 limit) { 
            return new Vector2( RandomFloat * limit.X, RandomFloat * limit.Y); 
        } 

        /// <summary>A List of stars</summary> 
        ///public static List<UiElement> Stars = new List<UiElement>();

        /// <summary>Allows 2D Drawing</summary> 
        public static SpriteBatch SpriteBatch;

        /// <summary>A List of stars</summary> 
        public static UiElements Stars = new UiElements();

        /// <summary>A reference to all images in the game</summary> 
        public static Dictionary<string, Texture2D> Images = new Dictionary<string, Texture2D>();

        /// <summary>A reference to all sounds in the game</summary> 
        public static Dictionary<string, SoundEffect> Sounds = new Dictionary<string, SoundEffect>();
        public static Dictionary<string, Song> Songs = new Dictionary<string, Song>();

        /// <summary>A List of player shots</summary> 
        public static UiElements PlayerShots = new UiElements(); 

        /// <summary>The state of the keys now</summary> 
        public static KeyboardState KeyboardNow = Keyboard.GetState();

        /// <summary>The state of the keys now</summary> 
        public static KeyboardState KeyboardOld = Keyboard.GetState(); 
        public static bool CheckFirstKeyPress(Keys key) { 
            if (KeyboardNow.IsKeyDown(key) && KeyboardOld.IsKeyUp(key)) {
                return true; 
            } return false; 
        }

        /// <summary>Manages the particles</summary> 
        public static UiElements Particles = new UiElements(); 

        //DEPRECATED
        //public static UiElement CreateParticle(Vector2 location, Vector2 velocity) { 
        //    UiElement particle = Particles.Add(); 
        //    particle.Location = location; particle.Image = Images["Particle01"]; 
        //    particle.Modifiers.Add(Modifiers.DeleteOffScreenModifier.Default); 
        //    particle.Velocity = velocity; 
        //    return particle; 
        //}

        public static Color RandomColor { 
            get 
            { 
                return new Color(RandomFloat, RandomFloat, RandomFloat, RandomFloat); 
            } 
        }

        public static Color RedColor
        {
            get
            {
                return new Color(255, RandomFloat, RandomFloat, RandomFloat);
            }
        }


        public static UiElement CreateFadingParticle(Vector2 location, Vector2 velocity) { 
            UiElement particle = Particles.Add(); 
            particle.Location = location; 
            particle.Image = Images["Particle01"]; 
            particle.Modifiers.Add(Modifiers.FadeOutModifier.DefaultFast); 
            //particle.Color = RandomColor;

            particle.Color = RedColor;
            particle.Velocity = velocity; 
            particle.Size *= 2; 
            return particle; 
        }

        public static UiElements Enemies = new UiElements(); 

        public static UiElement CreateEnemy1() 
        { 
            UiElement enemy = Enemies.Add();
            enemy.Image = Images["Enemy01"];
            enemy.Location = RandomVector2(ScreenSize);
            enemy.Location.Y = 0;
            enemy.Velocity.X = 2;
            enemy.Modifiers.Add(Modifiers.RegionBounceModifier.ScreenDefault);
            enemy.Modifiers.Add(Modifiers.EnemyShootingModifier.Create(100, 50));
            return enemy; 
        } 

        public static UiElement CreateEnemy2() 
        { 
            // starts out just like enemy 1 
            UiElement enemy = CreateEnemy1(); 
            // change the image 
            enemy.Image = Images["Enemy02"]; 
            //horizontal Speed = atleast Enemy 1 + up to 2 more 
            enemy.Velocity.X = 2 + RandomFloat * 2;
            //vertical speed from -1 to +1 adds diagonals 
            enemy.Velocity.Y = 1 - (RandomFloat * 2); 
            enemy.RemoveModifier(Modifiers.RegionBounceModifier.ScreenDefault.GetType()); 
            enemy.Modifiers.Add(Modifiers.RegionBounceModifier.TopHalfDefault); 
            return enemy; 
        }

        public static UiElement CreateEnemy3()
        {
            // starts out just like enemy 1 
            UiElement enemy = CreateEnemy2();
            // change the image 
            enemy.Image = Images["Enemy03"];
            //horizontal Speed = atleast Enemy 2 + up to 3 more 
            enemy.Velocity.X = 2 + RandomFloat * 3;
            //vertical speed from -1 to + 1 adds diagonals 
            enemy.Velocity.Y = 1 - (RandomFloat * 3);
            return enemy;
        }

        public static float Distance(Vector2 location1, Vector2 location2) 
        { 
            Vector2 diff = location1 - location2; 
            float h = Math.Abs(diff.X * diff.X); 
            float w = Math.Abs(diff.Y * diff.Y); 
            return (float)Math.Sqrt(h + w); 
        }
        
        public static float Distance(UiElement element1, UiElement element2) 
        { return Distance(element1.Center, element2.Center); }

        public static Vector2 RandomVector2() 
        { 
            return new Vector2(RandomFloat, RandomFloat); 
        } 
        
        public static void CreateParticleExplosion(Vector2 location, int sparkCount) 
        { 
            for (int i = 0; i < sparkCount; i++) 
            { 
                Vector2 movement = RandomVector2(); 
                movement.X -= 0.5f; 
                movement.Y -= 0.5f; 
                UiElement el = CreateFadingParticle(location, movement); 
                el.Size *= 1 + (2 * RandomFloat); 
            } 
        } 
        
        public static void HandleEnemyImpact(UiElement shot, UiElement enemy) 
        { 
            shot.DeleteMe = true; 
            enemy.DeleteMe = true; 
            
            //1st type
            //CreateParticleExplosion(enemy.Center, 100); 
            //CreateParticleExplosion(shot.Center, 20);

            //2nd type
            //Particles.AddRange(CreatePixelPerfectExplosion(enemy)); 
            //Particles.AddRange(CreatePixelPerfectExplosion(shot));
            
            //3d type
            UiElements explosion = CreatePixelPerfectExplosion(enemy); 
            explosion.AlterVelocityToAll(shot.Velocity / 5); 
            Particles.AddRange(explosion);
            //MediaPlayer.Play(Access.Sounds["explosion"]);
            Access.Sounds["explosion"].Play();
            Access.Player.Score += 10;
        }

        /// <summary>Turn an element into particles</summary> 
        /// <remarks>only adds visible pixels, maintains velocity</remarks> 
        public static UiElements ImageToParticles(Texture2D original) 
        { 
            UiElements elements = new UiElements(); 
            Color[] colorParts = new Color[original.Width * original.Height]; 
            //fills the array with the colors of texture
            original.GetData(colorParts); 
            for (int x = 0; x < original.Width; x++) 
            { 
                for (int y = 0; y < original.Height; y++) 
                { 
                    Color color = colorParts[x + y * original.Width]; 
                    if (color.A == 0) continue; 
                    UiElement element = elements.Add(); 
                    element.Location = new Vector2(x, y); 
                    element.Color = color; element.Image = Images["Particle01"]; 
                } 
            } 
            return elements; 
        }

        //desintegration
        public static UiElements CreatePixelDesintegration(UiElement original) 
        { 
            UiElements orig = ImageToParticles(original.Image); 
            orig.AddModifierToAll(new FrictionModifier() { Friction = 0.02f }); 
            orig.AddModifierToAll(new FadeOutModifier() { Reducer = 2 }); 
            orig.AddModifierToAll(WobbleModifier.Default); 
            orig.MoveAll(original.Location); 
            orig.AlterVelocityToAll(original.Velocity); 
            return orig; 
        }

        //blow to pieces
        public static UiElements CreatePixelBlowToPieces(UiElement original) 
        { 
            UiElements orig = ImageToParticles(original.Image); 
            UiElements newParts = new UiElements(); 
            int parts = Random.Next(3) + 5; 
            // = random between 5 and 8 
            orig.AddModifierToAll(FadeOutModifier.Default); 
            orig.MoveAll(original.Location); 
            for ( int i = 0; i < parts; i++) 
            { 
                Vector2 position = RandomVector2(original.Size)+original.Location; 
                float Radius = original.Size.X / (parts - i); 
                //Vector2 velocityMod = RandomVector2();
                Vector2 velocityMod = (position - original.Center) / 25;
                for(int p = orig.Count() - 1; p > -1; p--) 
                { 
                    UiElement part = orig[p];
                    float distance = Distance(position, part.Location); 
                    float radiusVariance = Radius + (RandomFloat * 3); 
                    if (distance < radiusVariance) 
                    { 
                        orig.Remove(part); 
                        newParts.Add(part); 
                        part.Velocity = original.Velocity + velocityMod; 
                    } 
                } 
            } 
            return newParts; 
        }

        //burn it with fire
        public static UiElements CreatePixelPerfectExplosion(UiElement original) 
        { 
            UiElements orig = ImageToParticles(original.Image); 
            UiElements newParts = new UiElements(); 
            int parts = Random.Next(3) + 5; 
            // = random between 2 and 5 
            orig.AddModifierToAll(FadeOutModifier.DefaultFast); 
            orig.MoveAll(original.Location); for ( int i = 0; i < parts; i++) 
            { 
                Vector2 position = RandomVector2(original.Size)+original.Location; 
                float Radius = original.Size.X / (parts - i); 
                Vector2 velocityMod = (position - original.Center) / 25; 
                for(int p = orig.Count() - 1; p > -1; p--) 
                { 
                    UiElement part = orig[p]; 
                    float distance = Distance(position, part.Location); 
                    float radiusVariance = Radius + (RandomFloat * 3); 
                    if (distance < radiusVariance) 
                    { 
                        orig.Remove(part); 
                        newParts.Add(part); 
                        part.Velocity = original.Velocity + velocityMod; 
                        if (distance > radiusVariance -0.5f && distance < Distance(position, original.Center)) 
                        { part.Modifiers.Add(new FlameTrailModifier()); }
                    } 
                } 
            } 
            return newParts; 
        }

        /// <summary>A list of all enemy shots</summary> 
        public static UiElements EnemyShots = new UiElements();

        public static float RandomFloatWithNegative 
        {
            get { return ((float)Random.NextDouble() - 0.5f) * 2; } 
        }
    }
}
