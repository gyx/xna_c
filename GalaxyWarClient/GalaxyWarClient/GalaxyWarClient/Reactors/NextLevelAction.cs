﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    class NextLevelAction : IAction
    {
        public static NextLevelAction Default = new NextLevelAction();
        public void Execute()
        {
            Access.Levels.CurrentLevel++;
        }
    }
}
