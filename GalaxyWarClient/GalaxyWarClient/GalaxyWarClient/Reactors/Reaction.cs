﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    class Reaction
    {
        public ICondition Condition;
        public IAction Action;
    }
}
