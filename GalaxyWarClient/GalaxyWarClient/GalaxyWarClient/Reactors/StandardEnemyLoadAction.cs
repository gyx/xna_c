﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    class StandardEnemyLoadAction : IAction
    {
        public static StandardEnemyLoadAction Default = new StandardEnemyLoadAction();

        public void Execute()
        {
            Access.Levels.CurrentLevel++;
            int e2 = Access.Levels.CurrentLevel / 4;
            int e1 = Access.Levels.CurrentLevel - (e2 * 4);
            for (int i = 0; i < e1; i++)
            {
                Access.CreateEnemy1();
            }
            for (int i = 0; i < e2; i++)
            {
                Access.CreateEnemy2();
            }
        }
    }
}
