﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    class NoEnemiesLeftCondition : ICondition
    {
        public static NoEnemiesLeftCondition Default = new NoEnemiesLeftCondition();
        public bool IsSatisfied
        {
            get
            {
                if (Access.Enemies.Count == 0
                    && Access.EnemyShots.Count == 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
