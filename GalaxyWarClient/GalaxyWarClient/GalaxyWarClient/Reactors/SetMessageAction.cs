﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GalaxyWarClient.Reactors
{
    class SetMessageAction : IAction
    {
        private string Text;
        private Color Color;
        public SetMessageAction(string text, Color color)
        {
            this.Text = text;
            this.Color = color;
        }
        public void Execute()
        {
            Access.Display.CentralColor = Color;
            Access.Display.CentralMessage = Text;
        }
    }
}
