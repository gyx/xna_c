﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GalaxyWarClient.Reactors
{
    class ReactorCore
    {
        public List<Reaction> Reactions
        = new List<Reaction>();
        public Reaction Final
        = new Reaction()
        {
            Condition = NoEnemiesLeftCondition.Default,
            Action = new SetMessageAction("You Won!", Color.LightGreen)
        };
        public void React()
        {
            if (Reactions.Count > 0)
            {
                Reaction react = this.Reactions.First();
                if (react.Condition.IsSatisfied)
                {
                    this.Reactions.Remove(react);
                    react.Action.Execute();
                }
            }
            else
            {
                if (Final.Condition.IsSatisfied)
                {
                    Final.Action.Execute();
                }
            }
        }
    }
}
