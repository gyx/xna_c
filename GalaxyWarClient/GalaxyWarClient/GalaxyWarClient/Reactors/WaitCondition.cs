﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    class WaitCondition : ICondition
    {
        public static int DefaultWait = 60;
        public static WaitCondition Default
        {
            get
            {
                return new WaitCondition(DefaultWait);
            }
        }
        private int frameCount;
        public WaitCondition(int frameCount)
        {
            this.frameCount = frameCount;
        }
        public bool IsSatisfied
        {
            get
            {
                if (this.frameCount-- < 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
