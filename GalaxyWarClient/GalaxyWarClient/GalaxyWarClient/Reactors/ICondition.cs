﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    interface ICondition
    {
        bool IsSatisfied { get; }
    }
}
