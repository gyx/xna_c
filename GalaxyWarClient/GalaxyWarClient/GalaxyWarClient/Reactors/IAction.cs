﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient.Reactors
{
    interface IAction
    {
        void Execute();
    }
}
