﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient
{
    partial class Access 
    { 
        public class Display 
        {
            //public static UiMenuItem MenuItem;
            public static bool InMenu = true;
            public static SpriteFont MainFont; 
            public static Vector2 ScoreLocation = new Vector2(0, 455);
            public static Vector2 LevelLocation = new Vector2(125, 455);
            public static Vector2 HealthLocation = new Vector2(750, 455); 
            public static Vector2 LivesLocation = new Vector2(600, 455);
            public static Vector2 CentralSpot = Access.ScreenSize / 2 - Access.RandomVector2() * 20;
            public static string CentralMessage = "";
            public static Color CentralColor = Color.Red;
            public static MouseState MouseNow = Mouse.GetState();
            public static MouseState MousePrevious = Mouse.GetState();

            public static Vector2 MouseLocation 
            { 
                get 
                { 
                    return new Vector2(MouseNow.X, MouseNow.Y); 
                } 
            } 
            
            public static Texture2D MouseCursor = null;

            public static void Paint() 
            { 
                Access.SpriteBatch.DrawString(MainFont, 
                    "Score: " + Access.Player.Score.ToString(), 
                    ScoreLocation, 
                    Color.LightYellow);

                Access.SpriteBatch.DrawString(MainFont, 
                    "Level: " + Access.Levels.CurrentLevel.ToString(), 
                    LevelLocation, 
                    Color.LightYellow);

                int Green = 55 + Access.Player.Health * 2; 
                int Red = 255 - Access.Player.Health * 2;
                Access.SpriteBatch.DrawString(MainFont, 
                    Access.Player.Health + "%", 
                    HealthLocation, 
                    new Color(Red, Green, 55));
                
                Access.SpriteBatch.DrawString(MainFont, 
                    "Lives: " + Access.Player.Lives, 
                    LivesLocation, 
                    Color.White);

                Access.SpriteBatch.DrawString(MainFont, 
                    CentralMessage, 
                    CentralSpot, 
                    CentralColor);
            }

            private static UiElements menu;

            public static UiElements Menu
            {
                get
                {
                    if (menu == null)
                    {
                        menu = new UiElements();
                        UiMenuItem mi = new UiMenuItem("continue", "Resume Game");
                        mi.Clicked += new UiMenuItem.ClickHandler(ReactToItemClicked);
                        menu.Add(mi);

                        mi = new UiMenuItem("newgame", "New Game");
                        mi.Clicked += new UiMenuItem.ClickHandler(ReactToItemClicked);
                        menu.Add(mi);

                        mi = new UiMenuItem("exit", "Exit");
                        mi.Clicked += new UiMenuItem.ClickHandler(ReactToItemClicked);
                        menu.Add(mi);
                        UiMenuItem.AdjustMenus(menu);
                    }
                    return menu;
                }
            }

            public static void ReactToItemClicked(string key)
            {
                if (key == "continue") Access.Display.InMenu = false;
                if (key == "exit") Access.CurrentGame.Exit();
                if (key == "newgame")
                {
                    Player.Create();
                    Player.Health = 100;
                    Player.Lives = 3;
                    Player.Score = 0;
                    Access.PlayerShots.Clear();
                    Access.EnemyShots.Clear();
                    Access.Enemies.Clear();
                    Levels.CurrentLevel = 0;
                    CentralMessage = ""; // gets rid of Game Over text
                    Access.Display.InMenu = false;
                }
            }
        } 
    }
}
