﻿using GalaxyWarClient.Modifiers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GalaxyWarClient
{
    /// <summary> /// manages a basic visual element /// </summary> 
    public class UiElement
    {
        /// <summary>The texture to draw</summary>
        private Texture2D image;

        /// <summary>The texture to draw</summary>
        //DEPRECATED
        //public Texture2D Image
        //{
        //    get { return this.image; }
        //    set
        //    {
        //        this.image = value; this.Size = Access.ImageSize(this.image);
        //    }
        //}

        /// <summary>a rectangle defining the size and location of the ship</summary>
        public Rectangle Rectangle { 
            get {
                return new Rectangle(
                    (int)this.Location.X, 
                    (int)this.Location.Y, 
                    (int)this.Size.X, 
                    (int)this.Size.Y); 
            } 
        }

        /// <summary>The Size of the Image</summary>
        private Vector2 size = Vector2.Zero; 
        /// <summary>The Size of the Image</summary> 
        public Vector2 Size 
        { 
            get 
            { 
                return this.size; 
            } 
            set 
            { 
                this.size = value;
                //this.ImpactRadius = (value.X + value.Y) / 2;
                this.ImpactRadius = (value.X + value.Y) / 3;
            } 
        }

        
        /// <summary>The location of the element</summary> 
        public Vector2 Location = Vector2.Zero;
        /// <summary>The Color Mask to use when drawing</summary> 
        public Color Color = Color.White;

        /// <summary>The offset amount in location per update</summary> 
        public Vector2 Velocity = Vector2.Zero;

        /// <summary>Max change in speed per update</summary> 
        public float Impulse = 0.2f;
 
        /// <summary>a list of all the modifying effects</summary> 
        public List<IModifier> Modifiers = new List<IModifier>(); 
        
        /// <summary>Updates the UiElement</summary> 
        virtual public void Modify() { 
            this.Location += this.Velocity; 
            foreach (IModifier mod in this.Modifiers) { 
                mod.Modify(this); 
            } 
        }

        //remove modifier by type
        public void RemoveModifier(Type modifierType) 
        { 
            List<IModifier> toRemove = new List<IModifier>(); 
            foreach (IModifier mod in this.Modifiers) 
            { 
                if (mod.GetType() == modifierType) 
                { 
                    toRemove.Add(mod); 
                } 
            } 
            foreach (IModifier mod in toRemove) 
            { 
                this.Modifiers.Remove(mod); 
            } 
        }

        /// <summary>Impact</summary> 
        public float ImpactRadius = 10;
        /// <summary>Target the center of the ship</summary> 
        public Vector2 Center
        { 
            get { return this.Size / 2 + this.Location; } 
        } 
        /// <summary>The texture to draw</summary> 
        public Texture2D Image 
        { 
            get { return this.image; } 
            set 
            { 
                this.image = value; 
                this.Size = Access.ImageSize(this.image); 
                this.ImpactRadius = (this.Size.X + this.Size.Y) / 4; 
            } 
        }

        //public void Paint() { 
        //    Access.SpriteBatch.Draw(this.image, this.Rectangle, this.Color); 
        //}

        /// <summary>This will mark it for removal from Lists.</summary> 
        public bool DeleteMe = false;

        public static float Disorientation = 0; 

        virtual public void Paint() 
        { 
            if (Disorientation < 0.1f) 
            { Access.SpriteBatch.Draw(this.image, this.Rectangle, this.Color); } 
            else 
            { 
                Rectangle rect = this.Rectangle; 
                rect.X += (int)(Access.RandomFloatWithNegative * Disorientation); 
                rect.Y += (int)(Access.RandomFloatWithNegative * Disorientation); 
                Access.SpriteBatch.Draw(this.image, rect, this.Color); 
            }
        }
    }
}
